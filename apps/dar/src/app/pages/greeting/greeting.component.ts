import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'dar-lab-ng-greeting',
  templateUrl: './greeting.component.html',
  styleUrls: ['./greeting.component.scss']
})
export class GreetingComponent implements OnInit {
  
  public ProjectName = "";
  public greetingText = "Welcome to";

  constructor() { }

  ngOnInit(): void {
    console.log('App Component!');
    this.ProjectName = 'DarLab';
  }

  sayHello() {
    this.greetingText = 'Hello';
  }
}
