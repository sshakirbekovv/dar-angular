import { Component } from '@angular/core';

@Component({
    selector: 'button-primary',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.scss'],
})

export class ButtonComponent {
     textButtonPrimary = 'Login';
}
