import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Category } from '@dar-lab-ng/api-interfaces';

@Component({
  selector: 'dar-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent{

  @Input()
  categories: Category[] = [];

  @Output()
  rowClicked = new EventEmitter<Category>();


  rowClickHandler(category: Category) {
    this.rowClicked.emit(category);
  }

}
