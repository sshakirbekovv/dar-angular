import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Category } from '@dar-lab-ng/api-interfaces';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CategoriesService } from '../categories.service';

@Component({
  selector: 'dar-category',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  categories$: Observable<Category[]>;

  searchTerm = '';

  constructor (
    private router: Router,
    private categoriesService: CategoriesService,
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData () {
    this.categories$ = this.categoriesService.getCategories().pipe(
    map(
        categories => this.searchTerm ? categories.filter(
          c => c.title.toLowerCase().includes(this.searchTerm.toLowerCase())) : categories)
    )
  }

  rowClickHandler(category: Category) {
    this.router.navigate(['categories', category.id]);
  }

  modelChanges () {
    console.log(this.searchTerm);
  }

  onSearchClick() {
     this.getData();
  }

}

