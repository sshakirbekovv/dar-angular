
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators } from '@angular/forms';
import { Category } from '@dar-lab-ng/api-interfaces';

@Injectable({ providedIn: 'root' })

export class CategoriesService {

    constructor (
        private httpClient: HttpClient,
    ) { }
   
    createCategoryForm () {
        const form = new FormGroup({
            title: new FormControl('', [Validators.required, Validators.minLength(3)] ),
            sort: new FormControl('', Validators.required),
            is_published: new FormControl(false),
            article_id: new FormControl(''),
            tags: new FormArray([])
        });

        return form;
    }

    getCategories () {
        return this.httpClient.get<Category[]>(`/api/categories`)
    }
    
    getCategory (id: string) {
        return this.httpClient.get<Category>(`/api/categories/${id}`)
    }

    pathCategoryForm(category: Category, form: FormGroup) {
        form.patchValue(category);
        category.tags.forEach(tag => {
          const tagsArray = form.controls.tags as FormArray;
          const tagControl = new FormGroup({
            name: new FormControl(tag.name)
          });
          tagsArray.push(tagControl);
        })
    }

    addTag(form: FormGroup) {
        const tagsArray = form.controls.tags as FormArray;
        const tagControl = new FormGroup({
          name: new FormControl('')
        });
        tagsArray.push(tagControl);
    }

    removeTag(index: number, form: FormGroup) {
        const tagsArray = form.get('tags') as FormArray;
        tagsArray.removeAt(index);
    }

    createCategory(data: Partial<Category>) {
        return this.httpClient.post<Category>(`/api/categories`, data);
    }

    updateCategory(id:string, data: Partial<Category>) {
       return this.httpClient.put<Category>(`/api/categories/${id}`, data);
    }
    
    deleteCategory(id:string) {
        return this.httpClient.delete(`/api/categories/${id}`);
    }

}