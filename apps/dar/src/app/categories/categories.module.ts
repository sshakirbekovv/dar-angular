import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoryResolver } from './category.resolver';
import { CategoriesComponent } from './categories/categories.component';
import { CategoryComponent } from './category/category.component';
import { CategoryListComponent } from './category-list/category-list.component';
import { CategoryFormComponent } from './category-form/category-form.component';
import { CategoryCreateComponent } from './category-create/category-create.component';



@NgModule({
  declarations: [
    CategoriesComponent,
    CategoryListComponent,
    CategoryComponent,
    CategoryFormComponent,
    CategoryCreateComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    CategoriesRoutingModule,
  ],
  providers: [
    CategoryResolver
  ]
})
export class CategoriesModule { }
