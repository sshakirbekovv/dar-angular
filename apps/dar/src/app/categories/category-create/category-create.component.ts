import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Category, Article } from '@dar-lab-ng/api-interfaces';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { ArticlesService } from '../../articles/articles.service';
import { CategoriesService } from '../categories.service';

@Component({
  selector: 'dar-category-create',
  templateUrl: './category-create.component.html',
  styleUrls: ['./category-create.component.scss']
})
export class CategoryCreateComponent implements OnInit {

  category: Category;
  articles$: Observable<Article[]>;

  form: FormGroup;

  constructor(
   private articleService: ArticlesService,
   private categoriesService: CategoriesService,
   private router: Router,
  ) { }

  ngOnInit(): void {

    this.form = this.categoriesService.createCategoryForm();
    this.articles$ = this.articleService.getArticles(5, 'id:DESC');
    
  }

  addTag() {
     this.categoriesService.addTag(this.form);
  }

  onSubmit() {
    console.log(this.form.valid);
    if (!this.form.valid) {
      return;
    }
    this.categoriesService.createCategory(this.form.value)
    .pipe(
      catchError(err => {
        console.error(err);
        return of(null);
      })
    )
    .subscribe(res => {
        console.log(res);
        if (res && res.id) {
          this.router.navigate(['/categories'])
        }
    })

  }

  removeTag(index: number) {
    this.categoriesService.removeTag(index, this.form);
  }

      

}
