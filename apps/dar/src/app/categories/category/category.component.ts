import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Article, Category } from '@dar-lab-ng/api-interfaces';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { ArticlesService } from '../../articles/articles.service';
import { CategoriesService } from '../categories.service';

@Component({
  selector: 'dar-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  category: Category;
  articles$: Observable<Article[]>;

  form: FormGroup;
  id: string;

  constructor(
   private route: ActivatedRoute,
   private articleService: ArticlesService,
   private categoriesService: CategoriesService,
   private router: Router,
  ) { }

  ngOnInit(): void {

    this.form = this.categoriesService.createCategoryForm();

    this.route.data.subscribe(data => {
      this.category = data.category;
    }),
    tap((category: Category | null) => {
      if (category) {
        this.id = category.id;
        this.categoriesService.pathCategoryForm(category, this.form);
      }
    })

    this.articles$ = this.articleService.getArticles(5, 'id:DESC');
  }

  addTag() {
     this.categoriesService.addTag(this.form);
  }

  onSubmit() {
    console.log(this.form.valid);
    if (!this.form.valid && this.id) {
      return;
    }

    this.categoriesService.updateCategory(this.id, this.form.value)
    .pipe(
      catchError(err => {
        console.error(err);
        return of(null);
      })
    )
    .subscribe(res => {
      console.log(res)
      if (res && res.id) {
        this.router.navigate(['/categories'])
      }
    })

  }

  removeTag(index: number) {
    this.categoriesService.removeTag(index, this.form);
  }

}
