import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Article} from '@dar-lab-ng/api-interfaces';

@Component({
  selector: 'dar-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.scss']
})
export class CategoryFormComponent {

  @Input()
  form: FormGroup;

  @Input()
  articles: Article[];

  @Output()
  formSubmit =  new EventEmitter<FormGroup>();

  @Output()
  addTag =  new EventEmitter();

  @Output()
  removeTag =  new EventEmitter<number>();

  submitted = false;

  onSubmitHandler () {
    this.submitted = true;
     this.formSubmit.emit(this.form);
  }
  
  onRemoveTag (index: number) {
    this.removeTag.emit(index);
  }

  onAddTag () {
    this.addTag.emit();
  }

}
