import { Component } from '@angular/core';

@Component({
    selector: 'dar-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss'],
})

export class FooterComponent {
    items = ["О нас", "Галерея", "DAR Lab", "Медиа"];
    itemsFooter = ["Lorem", "Lorem", "Lorem", "Lorem"];
    socials = ["fab fa-instagram", "fab fa-facebook", "fab fa-twitter", "fab fa-telegram"];
}
