import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message } from '@dar-lab-ng/api-interfaces';

@Component({
  selector: 'dar-lab-ng-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  hello$ = this.http.get<Message>('/api/hello');
  
  public ProjectName = "";
  public greetingText = "Welcome to";

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    console.log('App Component!');
    this.ProjectName = 'DarLab';
  }

  sayHello() {
    this.greetingText = 'Hello';
  }
}
