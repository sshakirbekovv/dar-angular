import { Component } from '@angular/core';

@Component({
    selector: 'dar-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
})

export class HeaderComponent {
    items = ["Главное", "DAR lab", "О нас", "Медиа", "Новости", "Контакты"];
}
