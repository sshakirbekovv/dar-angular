import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { ArticlesComponent } from './articles/articles.component';
import { ArticleComponent } from './article/article.component';
import { ArticleResolver } from './article.resolver';
import { ArticleCreateComponent } from './article-create/article-create.component';

const routes: Route[] = [ 
    {
        path: '',
        pathMatch: 'full',
        component: ArticlesComponent,
    },
    {
        path: 'create',
        component: ArticleCreateComponent,
    },
    {
        path: ':id',
        component: ArticleComponent,
        resolve: {
            article: ArticleResolver
        }
    }
]


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
 })

export class ArticlesRoutingModule {

}